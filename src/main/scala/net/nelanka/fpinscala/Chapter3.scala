package net.nelanka.fpinscala

object Chapter3 {

  // Ex 3.1: What will be the result of the following match expression?
  // Answer: 3 (3rd case)

  // Ex 3.2: Implement tail for List
  def tail[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case _ :: t => t
  }

  // Ex 3.3: Implement setHead for replacing first element of a List
  def setHead[A](l: List[A], a: A): List[A] = a :: tail(l)

  // Ex 3.4: Generalize tail to drop, which removes first n elements of a List
  def drop[A](l: List[A], n: Int): List[A] = l match {
    case _ if n == 0 => l
    case Nil => Nil
    case _ :: t => drop(t, n -1)
  }

  // Ex 3.5: Implement dropWhile, which removes elements from the List as long as they match the predicate
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case h :: t if f(h) => dropWhile(t, f)
    case _ => l
  }

  // Ex 3.6: Return a List of all but the last element of a given List
  def init[A](l: List[A]): List[A] = l match {
      case _ :: Nil => Nil
      case h :: t => h :: init(t)
  }
}
