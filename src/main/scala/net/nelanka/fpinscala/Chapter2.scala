package net.nelanka.fpinscala

import scala.annotation.tailrec

object Chapter2 {

  // Ex 2.1: Write a tail recursive Fibonnaci number function
  def fib(n: Int): Int = {
    @tailrec
    def loop(a: Int, b: Int, i: Int): Int =
      if (i == 1) b else loop(b, a + b, i - 1)

    if (n <= 0) 0 else loop(0, 1, n)
  }

  // Ex 2.2: Implement isSorted
  def isSorted[A](as: Array[A], ordered: (A, A) => Boolean): Boolean =
    as.zip(as.tail).forall { case (a, b) => ordered(a, b) }

  // Ex 2.3: Implement currying function
  def curry[A, B, C](f: (A, B) => C): A => (B => C) = a => f(a, _)

  // Ex 2.4: Implement uncurry function
  def uncurry[A, B, C](f: A => B => C): (A, B) => C = (a: A, b: B) => f(a)(b)

  // Ex 2.5: Implement the HOF that composes two functions
  def compose[A, B, C](f: B => C, g: A => B): A => C = a => f(g(a))
}
