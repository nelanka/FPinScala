package net.nelanka.fpinscala

import org.scalatest.{Matchers, FlatSpec}
import Chapter3._

class Chapter3Spec extends FlatSpec with Matchers {
  "tail" should "return the tail of a List" in {
    tail(List(1, 2, 3)) shouldBe List(2, 3)
    tail(List(1)) shouldBe Nil
    tail(List()) shouldBe Nil
  }

  "setHead" should "replace first element of a List" in {
    setHead(List(1, 2, 3), 7) shouldBe List(7, 2, 3)
    setHead(List(), 7) shouldBe List(7)
  }

  "drop" should "remove first n elements of a List" in {
    drop(List(1, 2, 3, 4), 2) shouldBe List(3, 4)
  }

  "dropWhile" should "removes elements from the List as long as they match the predicate" in {
    dropWhile(List(1, 2, 3, 4), { a: Int => a < 3 }) shouldBe List(3, 4)
  }

  "init" should "return a List of all but the last element of a given List" in {
    init(List(1, 2, 3, 4)) shouldBe List(1, 2, 3)
    init(List(1)) shouldBe Nil
  }
}
