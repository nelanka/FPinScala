package net.nelanka.fpinscala

import org.scalatest.{FlatSpec, Matchers}
import Chapter2._

class Chapter2Spec extends FlatSpec with Matchers {

  "Tail recursive fibonnaci number function" should "work" in {
    Array(0, 1, 1, 2, 3, 5, 8).zipWithIndex.foreach { case (expected, index) => fib(index) shouldBe expected }
  }

  "isSorted" should "check if an array is sorted" in {
    isSorted(Array(1, 2, 3), (a: Int, b: Int) => a <= b) shouldBe true
    isSorted(Array(3, 2, 1), (a: Int, b: Int) => a <= b) shouldBe false
    isSorted(Array(1, 2, 2), (a: Int, b: Int) => a <= b) shouldBe true
    isSorted(Array(1, 2, 2), (a: Int, b: Int) => a < b) shouldBe false
  }

  "curry function" should "work" in {
    val f: (Int, String) => Boolean = (i, s) => (s * i).length > 100
    val curried = curry(f)
    curried(1)("Hello") shouldBe false
    curried(25)("Hello") shouldBe true
  }

  "uncurry function" should "work" in {
    val f: Int => String => Boolean = (i: Int) => (s: String) => (s * i).length > 100
    val uncurried = uncurry(f)
    uncurried(1, "Hello") shouldBe false
    uncurried(25, "Hello") shouldBe true
  }

  "compose" should "combine two functions" in {
    val f: String => Boolean = (s: String) => s.length > 100
    val g: Int => String = (i: Int) => "Hello"*i
    val composed: (Int) => Boolean = compose(f, g)
    composed(1) shouldBe false
    composed(25) shouldBe true
  }
}
